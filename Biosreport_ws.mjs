import soap from "soap";

export const handler = async (event) => {
  try {
    const json = JSON.parse(event.body);
    
    const url = json.URL;
    
    const requestArgs = {
      strFileN: json.strFileN,
      strDB: json.strDB,
      strCLIENT: json.strCLIENT,
      strTR: json.strTR,
      strCR: json.strCR,
      strN1: json.strN1,
      strN3: json.strN3,
      strN1S: json.strN1S,
      strN1P: json.strN1P,
      strN2S: json.strN2S,
      strN2P: json.strN2P,
      strN3S: json.strN3S, 
      strN3P: json.strN3P,
      strN4S: json.strN4S,
      strN4P: json.strN4P,
      strPLANTA: json.strPLANTA,
      strCULT: json.strCULT,
      strUSUC: json.strUSUC,
      strUSUN: json.strUSUN,
      strOnlySQL: json.strOnlySQL,
      strDateFormat: json.strDateFormat,
      strDateSep: json.strDateSep,
      strParam: json.strParam
    };  
    
    const client = await soap.createClientAsync(url);
    
    const response = await client.BIOSReportAsync(requestArgs);
    
    const result = response[0];
    
    return {
      statusCode: 200,
      body: JSON.stringify(result)
    };
  } catch (error) {
      return {
        statusCode: error.statusCode,
        body: error.message
    };  
  }
    
};
